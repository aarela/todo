<?php

return [
    'failed' => 'Sisselogimine ebaõnnestus, palun proovi uuesti.',
    'throttle' => 'Palun proovi :seconds sekundi pärast uuesti.',
];
