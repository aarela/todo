<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Parool peab olema vähemalt 6 tähemärki pikk ja mõlema paroolivälja väärtus sama.',
    'reset' => 'Parool on lähtestatud.',
    'sent' => 'Parooli lähtestamise link e-posti saadetud.',
    'token' => 'Midagi läks nihu. :(',
    'user' => "Sellise e-posti aadressiga kasutajat pole.",

];