@extends('layouts.app') @section('content')
<div class="container">
  <div class="row justify-content-md-center mt-5">
    <div class="col-lg-5 col-md-8">
      <div class="card">
        <h3 class="card-header">Logi sisse</h3>
        <div class="card-body">
          <div class="row my-2">


            <div class="col">
              <p class="lead">
                Sul pole veel kontot? <a href="{{ route('register') }}">Registreeru</a> kasutajaks.
              </p>
            </div>

          </div>
          <form class="form-horizontal" method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}

            <div class="form-group row">
           

              <div class="col">
                <div class="md-form">
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
               <label for="email" class="">E-mail</label>
                </div>
                 @if ($errors->has('email'))
               <div class="invalid-feedback">
                  <strong>{{ $errors->first('email') }}</strong>
                </div>
                @endif
              </div>
            </div>

            <div class="form-group row">
             

              <div class="col">
                <div class="md-form">
                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required> 
                <label for="password" class="">Parool</label>
                </div>
                @if ($errors->has('password'))
                <div class="invalid-feedback">
                  <strong>{{ $errors->first('password') }}</strong>
                </div>
                @endif
              </div>
            </div>

            <div class="form-group row">
              <div class="col-lg-6">
                <div class="form-check">
                  <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" name="remember" {{ old('remember') ? 'checked' : '' }}> Mäleta mind
                                    </label>
                </div>
              </div>
            </div>

            <div class="form-group row">
              <div class="col">
                <button type="submit" class="btn btn-primary">
                                    Logi sisse
                                </button>

                <a href="{{ route('password.request') }}">
                                    Parool ununes?
                                </a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection