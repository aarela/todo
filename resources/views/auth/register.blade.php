@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-md-center mt-5">
        <div class="col-lg-5 col-md-8">
            <div class="card">
                <h4 class="card-header">Registreeru kasutajaks</h4>
                <div class="card-body">
                    <form role="form" method="POST" action="{{ url('/register') }}">
                        {!! csrf_field() !!}

                        <div class="form-group row">
                            

                            <div class="col">
                              <div class="md-form">
                                
                              
                                <input
                                       id="name"
                                        type="text"
                                        class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                        name="name"
                                        value="{{ old('name') }}"
                                        required
                                >
                              <label class="" for="name">Nimi</label>
                                </div>
                                @if ($errors->has('name'))
                                    <div class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                              </div>
                                @endif
                            </div>
                          
                        </div>

                        <div class="form-group row">
                            

                            <div class="col">
                              <div class="md-form">
                                <input
                                       id="email"
                                        type="email"
                                        class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                        name="email"
                                        value="{{ old('email') }}"
                                        required
                                >
                                <label for="email" class="">E-Mail</label>
                              </div>
                                @if ($errors->has('email'))
                                     <div class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                              </div>
                                @endif
                            </div>
                          
                        </div>

                        <div class="form-group row">
                           

                           <div class="col">
                              <div class="md-form">
                                <input
                                       id="pwd1"
                                        type="password"
                                        class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                        name="password"
                                        required
                                >
                                 <label for="pwd1" class="">Parool</label>
                             </div>
                                @if ($errors->has('password'))
                                    <div class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            

                            <div class="col">
                              <div class="md-form">
                                <input
                                       id="pwd2"
                                        type="password"
                                        class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}"
                                        name="password_confirmation"
                                        required
                                >
                                <label for="pwd2" class="">Parool uuesti</label>
                              </div>
                                @if ($errors->has('password_confirmation'))
                                    <div class="invalid-feedback">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-6 offset-lg-4">
                                <button type="submit" class="btn btn-primary">
                                    Registreeru
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
