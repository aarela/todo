<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" >

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }}</title>

  
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <link href="{{ asset('css/open-iconic-bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('css/mdb.min.css') }}" rel="stylesheet">
  <link href="{{ asset('css/validetta.min.css') }}" rel="stylesheet">
  <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>
@if ($errors)
  <style>
    .invalid-feedback{display:inline-block;}  
  
</style>
@endif
<body class="pattern-6">
  <div class="bg"></div>
  <div id="app">
    <nav class="navbar navbar-expand-lg navbar-dark rgba-teal-strong">
      <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
                <span class="oi oi-bolt"></span> {{ config('app.name', 'Laravel') }}
            </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

        <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
          <ul class="navbar-nav">
            @if (Auth::guest())
            <li class="nav-item"><a href="{{ route('login') }}" class="nav-link">Logi sisse</a></li>
            <li class="nav-item"><a href="{{ route('register') }}" class="nav-link">Registreeru</a></li>
            @else
            <li class="nav-item dropdown">
              <a href="#" class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ Auth::user()->name }}
                            </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                <a href="{{ route('logout') }}" class="dropdown-item" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  {{ csrf_field() }}
                </form>
              </div>
            </li>
            @endif
          </ul>
        </div>

      </div>
    </nav>
    

    @yield('content')
  </div>

  <script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
  <script src="{{ asset('js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('js/mdb.min.js') }}"></script>
  <script src="{{ asset('js/popper.min.js') }}"></script>
  <script src="{{ asset('js/validetta.min.js') }}"></script>



  <script>
    $("#newtodo").submit(function(e){
        e.preventDefault();
    });
    
$('body').on("click",".done",function(e){
  e.preventDefault();
  $.ajax({
        type: "GET",
        url: '{{ action("HomeController@updateTodoStatus") }}',
        data: {status:$(this).attr('data-status'),id:$(this).attr('data-id')},
        jsonpCallback: updatetodostatus,
        success: function() {
          console.log("Sent");
        }
      })
});   
    

    
$('body').on("click",".deleteTodo",function(e){
  e.preventDefault();
  var deleteTodo = confirm("Kindel, et soovid kustutada?");
if (deleteTodo) {
 $.ajax({
        type: "GET",
        url: '{{ action("HomeController@deleteTodo") }}',
        data: {id:$(this).parents('.row').first().find('.done').attr('data-id')},
        jsonpCallback: updatetodostatus,
        success: function() {
          console.log("Sent");
        }
      })
}
}); 
    


    function tasksDone(){
      setTimeout(function(){
    var done=(100/(parseInt($('[data-status="0"]').length)+parseInt($('[data-status="1"]').length)))*parseInt($('[data-status="1"]').length);
        $('.progress-bar').css('width',done+'%').attr('aria-valuenow',done);
},200);    
      
}
    

   $(function(){
    $('form').validetta({
      realTime: true,
        display : 'inline',
        errorTemplateClass : 'validetta-inline',
        onValid : function( event ) {
            event.preventDefault();
          $.ajax({
        type: "GET",
        url: '{{ action("HomeController@newTodo") }}',
        data: $(this.form).serialize(),
        jsonpCallback: newtodo,
        success: function() {
          console.log("Sent");
        }
      })
        }
   });
     tasksDone();
    });
 
    
   function tododeleted(data){
    $('[data-id="'+data.id+'"]').parents('.row').first().remove();
     tasksDone();
   }
    function updatetodostatus(data){
      if(data.status=='0'){
     $('[data-id="'+data.id+'"]').attr('data-status','0').children().addClass('oi-sun').removeClass('oi-task').parents('.row').removeClass('light-green-text').addClass('grey-text');
      }
      else if(data.status=='1')
        {
          $('[data-id="'+data.id+'"]').attr('data-status','1').children().addClass('oi-task').removeClass('oi-sun').parents('.row').removeClass('grey-text').addClass('light-green-text');
        }
      tasksDone();
    }

    function newtodo(data) {
      document.getElementById('newtodo').reset();
      $('<div class="row my-1 grey-text"><div class="col-1"><a class="done" data-status="0" data-id="'+data.id+'"><span class="oi oi-sun"></span></a> </div><div class="col">' + data.title + '</div><div class="col-1"><a class="deleteTodo orange-text" href="#"><span class="oi oi-trash"></span></a></div></div>').insertBefore('#newtodo');
      tasksDone();    
}

    
  </script>
</body>

</html>