@extends('layouts.app') @section('content')
<div class="container">
  <div class="row mt-5">
    <div class="col-md-6">
      <section class="form-dark">
      <div class="card card-inverse z-depth-1 rgba-black-strong">
       
<div class="card-body text-white">
        <h4 class="card-title">Minu tegemised</h4>

<div class="progress" >
  <div class="progress-bar bg-light-green" role="progressbar" style="width: 0%;height:25px;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
</div>
        @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
        @endif
  @if(!empty($todos))
@foreach ($todos as $todo)
  <div class="row my-1 {{ ($todo->status=='0')?'grey-text':'light-green-text'}}"><div class="col-1"><a class="done" data-id="{{$todo->id}}" data-status="{{$todo->status}}"><span class="oi {{$todo->status=='1'?'oi-task':'oi-sun'}}"></span></a>  </div><div class="col"><h5>{{ $todo->title }}</h5></div><div class="col-1"><a class="deleteTodo orange-text" href="#"><span class="oi oi-trash"></span></a></div></div>
@endforeach
  @endif
  
        <form id="newtodo" method="POST" action="#">
          {{ csrf_field() }}
          <div class="form-row md-form  align-items-center">

            <div class="col">
              <input class="form-control white-text" maxlength="30"  data-validetta="required" data-vd-message-required="Palun täida väli" type="text" name="title" placeholder="Lisa uus kirje">

            </div>

            <div class="col-auto">
              <button type="submit" class="btn btn-default newtodo">Lisa</button>

            </div>
          </div>
        </form>

        </div>

      </div>
      </section>
    </div>
  </div>
</div>
@endsection