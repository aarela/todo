<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::any('/new-todo', 'HomeController@newTodo');

Route::any('/update-todo-status', 'HomeController@updateTodoStatus');

Route::any('/delete-todo', 'HomeController@deleteTodo');


