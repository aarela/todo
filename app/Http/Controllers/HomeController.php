<?php

namespace App\Http\Controllers;


use Auth;
use Illuminate\Http\Request;
use App\Todo;

class HomeController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    
    public function index()
    {
       $todos=Todo::where('user_id',Auth::user()->id)->get();
        return view('home')->with('todos',$todos);
    }
  

  public function newTodo(Request $request)
    
  {
    $todo = new Todo;
        $todo->title=$request->input('title');
        $todo->user_id = Auth::user()->id;
        $todo->save();
    
    return response()
            ->json(['title' => $request->input('title'),'id'=>$todo->id])
            ->withCallback('newtodo');
  }
  
public function updateTodoStatus(Request $request)
    
  {
    $todo = Todo::find($request->input('id'));
        $todo->status=($request->input('status')=='1')?'0':'1';
        $todo->save();
    
    return response()
            ->json(['status' => $todo->status,'id'=>$todo->id])
            ->withCallback('updatetodostatus');
  }
  
public function deleteTodo(Request $request)
    
  {
    $todo = Todo::where('id',$request->input('id'))->delete();  
    return response()
            ->json(['id'=>$request->input('id')])
            ->withCallback('tododeleted');
  }
}
