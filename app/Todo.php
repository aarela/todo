<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Todo extends Model
{
    use SoftDeletes;
     protected $fillable = ['user_id,status,title'];
     protected $dates = ['created_at,updated_at,deleted_at'];
}
